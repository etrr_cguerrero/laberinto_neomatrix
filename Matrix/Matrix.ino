/*
 *             
 * drawPixel ( int16_t x, int16_t y, uint16_t color)
 */

#include  <Adafruit_GFX.h>
#include  <Adafruit_NeoMatrix.h>
#include  <Adafruit_NeoPixel.h>

#define PIN 5
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(10, 9, PIN,  // Parámetro 1 = ancho de la matriz NeoPixel // Parámetro 2 = altura de la matriz
  NEO_MATRIX_TOP     + NEO_MATRIX_LEFT+
  NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG,
  NEO_GRB            + NEO_KHZ800); 


const uint16_t colors[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255) };


#define EJE_Y 0
#define EJE_X 1

#define LEFT 5
#define RIGHT 4
#define UP 3
#define DOWN 2

#define L 0
#define U 1
#define R 2
#define D 3
#define NO_KEY 255

#define CTE_TIEMPO 250

unsigned char movimiento;
unsigned char pos_hor = 0;
unsigned char pos_ver = 0;

unsigned char contador = 0;
unsigned char mov_1, mov_2, mov_3, mov_4;

int xcord;
int ycord;


int laberinto_1[10][10][4] = {
  /*
   * L: Left, U: Up, R: Rigth, D: Down
   */
  /*L,U,R,D   L,U,R,D   L,U,R,D   L,U,R,D   L,U,R,D   L,U,R,D   L,U,R,D   L,U,R,D   L,U,R,D   L,U,R,D */
  {{0,0,1,0},{1,0,1,0},{1,0,1,0},{1,0,0,1},{0,0,1,1},{1,0,1,0},{1,0,0,1},{0,0,1,1},{1,0,0,1},{0,0,0,1}},
  {{0,0,1,1},{1,0,1,0},{1,0,1,0},{1,1,0,1},{0,1,0,1},{0,0,1,0},{1,1,0,0},{0,1,0,1},{0,1,0,1},{0,1,0,1}},
  {{0,1,1,1},{1,0,1,0},{1,0,0,1},{0,1,0,1},{0,1,0,1},{0,0,0,1},{0,0,1,1},{1,1,0,1},{0,1,1,0},{1,1,0,1}},
  {{0,1,1,0},{1,0,0,1},{0,1,0,1},{0,1,0,0},{0,1,0,1},{0,1,1,1},{1,1,1,0},{1,1,0,1},{0,0,0,1},{0,1,1,0}},
  {{0,0,0,1},{0,1,0,1},{0,1,1,0},{1,0,1,0},{1,1,0,0},{0,1,0,0},{0,0,1,1},{1,1,0,0},{0,1,1,0},{1,0,0,1}},
  {{0,1,0,1},{0,1,1,0},{1,0,0,1},{0,0,1,0},{1,0,1,0},{1,0,0,1},{0,1,1,0},{1,0,1,1},{1,0,1,0},{1,1,0,1}},
  {{0,1,0,1},{0,0,1,1},{1,1,0,0},{0,0,1,1},{1,0,0,1},{0,1,1,0},{1,0,1,0},{1,1,1,0},{1,0,0,0},{0,1,0,1}},
  {{0,1,1,1},{1,1,0,0},{0,0,1,1},{1,1,0,0},{0,1,0,1},{0,0,1,1},{1,0,1,0},{1,0,1,0},{1,0,0,1},{0,1,0,1}},
  {{0,1,0,1},{0,0,1,0},{1,1,0,1},{0,0,1,1},{1,1,0,0},{0,1,0,1},{0,0,1,1},{1,0,0,0},{0,1,0,1},{0,1,0,1}},
  {{0,1,1,0},{1,0,1,0},{1,1,0,0},{0,1,1,0},{1,0,1,0},{1,1,0,0},{0,1,1,0},{1,0,1,0},{1,1,1,0},{1,1,0,0}}};

void setup() {
  //Joystick
  Serial.begin(9600);
  for(int i=2; i<6; i++){
    pinMode(i, OUTPUT);
  }
//Matriz
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(40);
  matrix.setTextColor(colors[0]);
}


void loop() {
  
 matrix.drawPixel(0, 2, matrix.Color(0, 255, 0));
  /*Serial.print(analogRead(0));
  Serial.print("    ");
  Serial.println(analogRead(1));*/
  for(int j=2; j<6; j++){
    digitalWrite(j,0);
  }
//Posicion inicial



  //Serial.println(joystick());
  
  if(contador == 0) { mov_1 = joystick(); }
  if(contador == 1) { mov_2 = joystick(); }
  if(contador == 2) { mov_3 = joystick(); } 
  if(contador == 3) { mov_4 = joystick(); }

  delay(CTE_TIEMPO);

  contador ++;

  if(contador == 4) 
  {
    if ((mov_1 == mov_2) && (mov_1 == mov_3) && (mov_1 == mov_4))
    {
      movimiento = mov_1;
      Serial.println(movimiento);
    }
    contador = 0;
  }

  Serial.print(laberinto_1[pos_hor][pos_ver][L]);
  Serial.print(laberinto_1[pos_hor][pos_ver][U]);
  Serial.print(laberinto_1[pos_hor][pos_ver][R]);
  Serial.println(laberinto_1[pos_hor][pos_ver][D]);

  /*
   * Movimiento hacia la derecha
   */
   if((laberinto_1[pos_hor][pos_ver][R] == 1) && (movimiento == R))
   {
      Serial.println("Avance hacia la derecha");
      pos_hor ++;
   }
  /*
   * Movimiento hacia la izquieda
   */
   if((laberinto_1[pos_hor][pos_ver][L] == 1) && (movimiento == L))
   {
     Serial.println("Avance hacia la izquierda");
      pos_hor = pos_hor - 1;
   }
   /*
   * Movimiento hacia abajo
   */
   if((laberinto_1[pos_hor][pos_ver][D] == 1) && (movimiento == D))
   {
      Serial.println("Avance hacia abajo");
      pos_ver ++;
   }
   /*
   * Movimiento hacia arriba
   */
   if((laberinto_1[pos_hor][pos_ver][U] == 1) && (movimiento == U))
   {
      Serial.println("Avance hacia arriba");
      pos_ver = pos_ver - 1;
   }

   Serial.print("PH: ");
   Serial.println(pos_hor);
   Serial.print("PV: ");
   Serial.println(pos_ver);

   movimiento = NO_KEY;


  //delay(10);
}

unsigned char joystick (void)
{
  if(analogRead(EJE_X)<10){
    digitalWrite(LEFT,1);
    return L;
  }

  if(analogRead(EJE_X)>1020){
    digitalWrite(RIGHT,1);
    return R;
  }
    
  if(analogRead(EJE_Y)<10){
    digitalWrite(DOWN,1);
    return D;
  }
  
  if(analogRead(EJE_Y)>1020){
    digitalWrite(UP,1);
    return U;
  }

  else
  {
    return NO_KEY;  
  }
}
